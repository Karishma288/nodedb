const express = require("express");
const router = express.Router();

const ProductController = require("../Controllers/Product.Controller");
// const UserController = require('../Controllers/User.Controller')


// ROUTES FOR THE PRODUCT

router.get("/getUser", ProductController.getUser); // //Get a list of all products

router.post("/createUser", ProductController.createUser); //Create new User

router.get("/findUserById/:id", ProductController.findUserById); //Get a product by id

router.put("/updateAUser/:id", ProductController.updateAUser); //Update a product by id

router.delete("/deleteAUser/:id", ProductController.deleteAUser); //Delete a product by id




// ROUTES FOR THE USERS

// router.post('/createUser', UserController.createUser )

module.exports = router;
